class Api::V1::Dashboard::VenuesController < ApplicationController
	protect_from_forgery with: :null_session
	before_action :set_venues, only: [:index]

	def index
		if stale?(@venues)
			render :json => @venues.map { |venue| {
				id: venue.id
			}}.to_json
		end
	end

	def create
		@venue = Venue.new(venue_params)
		if @venue.save
			render :status => 200, :json => { :status => "success" }.to_json
		else
      Rails.logger.info(@venue.errors.messages.inspect)
			render :status => 501, :nothing => true
		end
	end

	private
	def venue_params
    params.require(:venue).permit(:name, :expiration_date)
  end

  def set_venues
    # @venues = Venue.where('updated_at >= ?', Time.now)
    @venues = Venue.all
  end

end
