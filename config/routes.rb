Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # API
	namespace :api do
		namespace :v1 do
			namespace :dashboard do
				namespace :venues do
					post 'new' => :create
					get '/' => :index
				end
			end
		end
	end
end
